#!/usr/bin/bash
root=$(dirname "$0")
find $root \( -path "$root*.ts" -o -path "$root*.tsx" \) -a \( -not -path "$root/frontend*/node_modules/*" \) -exec deno fmt {} \;
