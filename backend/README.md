# Table of Contents

[[_TOC_]]

# Backend SQL Structure

The SQL code in
[init_ischia.sql](https://gitlab.com/jane314/ischia/-/blob/main/backend/init_ischia.sql)
generates the backend SQL structure of Ischia.

# Backend API Endpoints

## `/tbl_info/${tbl_name}` (GET)

Get information about an Ischia table.

Method: GET

Response:

```typescript
{
	status: number;
	tbl_name: string;
	cols: {
		col_name: string;
		col_disp_name: string;
		type: string;
	}[];
	cmds: {
		type: string;
		args?: {
			[arg: string]: string;
		};
	}[];
}
```

## `/tbl_info/${tbl_name}` (POST)

Update information about an Ischia table.

Method: POST

Body:

```typescript
{
  tbl_name:
  string;
  cols: {
    col_name:
    string;
    col_disp_name:
    string;
    type:
    string;
  }
  [];
}
```

Response:

```typescript
{
  status:
  number;
}
```

## `/read`

Execute a command of 'read' type.

Method: GET

URL: `/read?body=${ body }`, where `body` is the encoded URI compoment of the JSON form of an object of the following type:

```typescript
{
  cmd_name: string;
  args: {
	[col_name: string]: string | number | null;
  };
}
```

Response:

```typescript
{
  status: number;
  res: {
	[col_name: string]: string | number;
  }[];
}
```

## `/write` (POST)

Execute a command of 'write' type.

Method: POST

Body: JSON form of:

```typescript
{
  cmd_name: string;
  args: {
	[col_name: string]: string | number | null;
  };
}
```

Response:

```typescript
{
  status:
  number;
}
```

# Status Codes

| Status Code | Description      |
| ----------- | ---------------- |
| -4          | SQL error        |
| -3          | Missing argument |
| -2          | Access denied    |
| -1          | Does not exist   |
| 0           | Success          |
