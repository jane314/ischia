import { DB, RowObject } from "https://deno.land/x/sqlite/mod.ts";
import { zip } from "https://gitlab.com/jane314/coding-sketchpad/-/raw/main/JaneStdLib/JaneStdLib.ts";

/*
 * Types
 */
type IArgs = {
  [key: string]: string;
};

function ExecIschiaCmd(
  db: DB,
  cmd_name: string,
  args: IArgs,
  cmd_type: string,
): {
  status: number;
  res: RowObject[];
} {
  const cmd_query = db.prepareQuery<
    [string, string],
    { cmd: string; type: string }
  >(
    "select cmd, type from ischia_cmds where cmd_name = :cmd_name;",
  );
  const cmd_res = cmd_query.allEntries({ "cmd_name": cmd_name });
  if (cmd_res.length === 0) {
    return {
      status: -1,
      res: [],
    };
  }
  try {
    const exec_query = db.prepareQuery(cmd_res[0].cmd);
    if (cmd_res[0].type === "read" && cmd_type === "read") {
      const exec_res = exec_query.allEntries(args);
      exec_query.finalize();
      return {
        status: 1,
        res: exec_res,
      };
    } else if (cmd_res[0].type === "write" && cmd_type === "write") {
      exec_query.execute(args);
      exec_query.finalize();
      return {
        status: 1,
        res: [],
      };
    } else {
      return {
        status: -2,
        res: [],
      };
    }
  } catch (e) {
    console.error(new Date(), e.message);
    return {
      status: -4,
      res: [],
    };
  }
}

export { ExecIschiaCmd };
