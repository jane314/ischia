import { DB } from "https://deno.land/x/sqlite/mod.ts";
import { zip } from "https://gitlab.com/jane314/coding-sketchpad/-/raw/main/JaneStdLib/JaneStdLib.ts";
import { parseJson, responseTemplates, server } from "./server/server.ts";
// should really move parseJson to stdlib
import { ExecIschiaCmd } from "./db_access/db_access.ts";

if (Deno.args.length === 0) {
  console.log("Need to supply Sqlite DB filepath.");
  Deno.exit();
}

const db = new DB(Deno.args[0]);

server(
  {
    GET: {
      "/read": (body_json: string) => {
        const body = parseJson(body_json, {});
        const args = body.args || {};
        if (body.hasOwnProperty("cmd_name")) {
          const res = ExecIschiaCmd(db, body["cmd_name"], args, "read");
          return responseTemplates.default(JSON.stringify(res));
        }
        return responseTemplates.default("no");
      },
    },
    POST: {
      "/write": (body_json: string) => {
        const body = parseJson(body_json, {});
        const args = body.args || {};
        if (body.hasOwnProperty("cmd_name")) {
          const res = ExecIschiaCmd(db, body["cmd_name"], args, "write");
          return responseTemplates.default(JSON.stringify(res));
        }
        return responseTemplates.default("no");
      },
    },
  },
);
