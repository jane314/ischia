create table if not exists 'ischia_cmds' ( 
	'cmd_name' text NOT NULL unique,	-- the command's codename
	'type' text,						-- the command's type -- 'read' / 'write'
	'scope' text,						-- the command's scope (what it affects) -- 'table' / 'row' / 'col'
	'target_table' text NOT NULL,		-- the table the command acts on
	'target_row_id' integer,				-- the key for target_table, which
										-- the command is a function of.
										-- If the command is of type 'row', it is a function of 
										-- target_row_id and args together.
	'cmd' text,							-- the command's prepared SQLite query
	'args' text,						-- arguments the prepared SQLite query needs.
										-- 		This is JSON of type string[]
	primary key('cmd_name'),
	foreign key('target_table') references 'ischia_tables(tbl_name)'
	foreign key('target_row_id') references 'ischia_cols(tbl_name)'
);
create table if not exists 'ischia_cols' (
	'id'	integer NOT NULL unique,
	'col_name' text,			-- the column's name in SQL
	'col_disp_name' text,		-- the column's display name
	'type'	text,				-- the column's type 
										-- 		'text' / 'textblob' / 'number' / 'dateiso8601'
	'tbl_name'	text,			-- the table the column is associated with	
	'order_rank'	integer,			-- determines how to display columns L to R by default
	primary key('id' autoincrement)
);
create table if not exists 'ischia_tables' (
	'tbl_name'	text unique,
	primary key('tbl_name')
);
insert into ischia_cols (id, tbl_name) values (1, NULL);
create table if not exists 'table01' (
	'id' integer not null unique,
	'str' text,
	primary key('id')
);
