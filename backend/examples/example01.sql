insert into ischia_cmds (cmd_name, type, scope, target_table, target_row_id, cmd, args)
    values  ('read01', 'read', 'table', 'table01', NULL, 'select * from table01;', '[]');
insert into ischia_cmds (cmd_name, type, scope, target_table, target_row_id, cmd, args)
	values ('insert01', 'write', 'table', 'table01', NULL,' insert into table01 (str) values (:str);', '["str"]');
insert into ischia_cmds (cmd_name, type, scope, target_table, target_row_id, cmd, args)
    values  ('read02', 'read', 'table', 'table01', NULL, 'select * from table01; select * from ischia_cmds;', '[]');
