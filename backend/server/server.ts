import { serve } from "https://deno.land/std@0.125.0/http/server.ts";

/*
 * Types
 */
type IResponseTemplateLookup = {
  default: (body_str: string) => Response;
  [key: string]: (body_str: string) => Response;
};

type IHandlerLookup = {
  GET: {
    [url_path: string]: (body_json: string) => Response;
  };
  POST: {
    [url_path: string]: (body_json: string) => Response;
  };
};

/*
 * Lookups
 */
const responseTemplates: IResponseTemplateLookup = {
  404: (body_str: string) =>
    new Response(body_str, {
      headers: {
        "Content-Type": "text/html",
        "Access-Control-Allow-Origin": "http://localhost:3000",
        "Access-Control-Allow-Methods": "GET, POST",
      },
      status: 404,
    }),
  json: (body_str: string) =>
    new Response(body_str, {
      headers: {
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "http://localhost:3000",
        "Access-Control-Allow-Methods": "GET, POST",
      },
      status: 200,
    }),
  "cors-ok": (body_str: string) =>
    new Response("", {
      headers: {
        "Content-Type": "text/plain",
        "Access-Control-Allow-Origin": "http://localhost:3000",
        "Access-Control-Allow-Methods": "GET, POST",
        "Access-Control-Allow-Headers":
          "Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers",
      },
      status: 200,
    }),
  default: (body_str: string) =>
    new Response(body_str, {
      headers: {
        "Content-Type": "text/plain",
        "Access-Control-Allow-Origin": "http://localhost:3000",
        "Access-Control-Allow-Methods": "GET, POST",
      },
      status: 200,
    }),
};

/*
 * Auxiliary functions
 */
function normalizePathname(pathname: string) {
  return pathname.replace(/\/$/, "");
}

function parseJson(json_str: string, default_val: any): any {
  try {
    const x = JSON.parse(json_str.slice(0, 1024));
    return x;
  } catch (e) {
    console.error(e.message);
    return default_val;
  }
}

/*
 * Server function
 */
function server(handlers: IHandlerLookup, port: number = 50000) {
  const handler = async (request: Request): Promise<Response> => {
    const url = new URL(request.url);
    const path = normalizePathname(url.pathname);
    if (request.method === "GET") {
      if (handlers.GET.hasOwnProperty(path)) {
        const body_json = url.searchParams.get("body") || '""';
        return handlers.GET[path](body_json);
      }
    } else if (request.method === "POST") {
      if (handlers.POST.hasOwnProperty(path)) {
        const body = await request.text();
        return handlers.POST[path](body);
      }
    }
    return responseTemplates["404"]("404, sorry");
  };
  return serve(handler, { port });
}

export { parseJson, responseTemplates, server };
