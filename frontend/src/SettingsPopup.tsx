import "./reusable/Popup.css";
import "./SettingsPopup.css";
import { forwardRef } from "react";

const SettingsPopup = forwardRef((props, ref) => {
  if (!props.if) {
    return null;
  }

  return (
    <div className="insertUpdatePopup popup">
      <input
        type="button"
        onClick={(e) => props.resetColWidths()}
        value="Reset Column Widths"
      />
      <div>
        <input
          id="insertUpdatePopupCancel"
          type="button"
          value="ok"
          onClick={(e) => props.setSettingsPopupOpen(false)}
        />
      </div>
    </div>
  );
});
export { SettingsPopup };
