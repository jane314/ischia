import "./reusable/Popup.css";
import "./InsertUpdatePopup.css";
import { forwardRef } from "react";

const InsertUpdatePopup = forwardRef((props, ref) => {
  if (!props.if) {
    return null;
  }

  return (
    <div className="insertUpdatePopup popup">
      <div>
        <span className="colName">Date</span>
        <input
          value={props.inputs.date}
          type="date"
          ref={ref}
          onInput={(e) => props.handleInsertUpdateInput(e, "date")}
        />
      </div>
      <div>
        <span className="colName">Title</span>
        <input
          value={props.inputs.title}
          type="text"
          onInput={(e) => props.handleInsertUpdateInput(e, "title")}
        />
      </div>
      <div>
        <span className="colName">Tags</span>
        <input
          value={props.inputs.tags}
          type="text"
          onInput={(e) => props.handleInsertUpdateInput(e, "tags")}
        />
      </div>

      <div>
        <span className="colName">More Info</span>
        <textarea
          value={props.inputs["more_info"]}
          onInput={(e) => props.handleInsertUpdateInput(e, "more_info")}
        >
        </textarea>
      </div>
      <div>
        <input
          id="insertUpdatePopupInsert"
          type="button"
          value="Insert"
          accessKey="i"
          onClick={props.handleInsert}
        />
        <input
          id="insertUpdatePopupCancel"
          type="button"
          value="Cancel"
          accessKey="c"
          onClick={(e) => props.setInsertUpdatePopupOpen(false)}
        />
      </div>
    </div>
  );
});

export { InsertUpdatePopup };
