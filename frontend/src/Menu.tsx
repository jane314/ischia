import "./Menu.css";

function Menu(props) {
  if (!props.if) {
    return null;
  }

  return (
    <div
      className="menulist"
      style={{ top: props.coords[0], left: props.coords[1] }}
    >
      {props.menuitems.map((entry, i) => (
        <span
          key={"menuitem-" + i}
          className="menulist-item"
          onClick={entry.onClick}
        >
          {entry.text}
        </span>
      ))}
    </div>
  );
}

export { Menu };
