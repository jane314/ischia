import { useEffect, useRef, useState } from "react";
import { InsertUpdatePopup } from "./InsertUpdatePopup.tsx";
import { SettingsPopup } from "./SettingsPopup.tsx";
import { TableView } from "./TableView.tsx";
import { Menu } from "./Menu.tsx";
import "./App.css";

function App() {
  // useEffect triggers
  const [rowUpdates, setRowUpdates] = useState(0);
  const [tableUpdates, setTableUpdates] = useState(0);
  // table data
  const [cols, setCols] = useState([]);
  const [rows, setRows] = useState([]);
  const [colWidths, setColWidths] = useState([]);
  // user inputs
  const [inputs, setInputs] = useState({
    date: new Date().toISOString().split("T")[0],
    title: "",
    tags: "",
    "more_info": "",
  });
  // UI state
  const [insertUpdatePopupOpen, setInsertUpdatePopupOpen] = useState(false);
  const [settingsPopupOpen, setSettingsPopupOpen] = useState(false);
  const [viewingMenu, setViewingMenu] = useState(false);
  const insertUpdateFocusRef = useRef(null);
  const settingsFocusRef = useRef(null);

  function setPopupState(state: string) {
    if (state === "insertUpdate") {
      setInsertUpdatePopupOpen(true);
      setSettingsPopupOpen(false);
    } else if (state === "settings") {
      setInsertUpdatePopupOpen(false);
      setSettingsPopupOpen(true);
    } else {
      setInsertUpdatePopupOpen(false);
      setSettingsPopupOpen(false);
    }
  }

  function resetColWidths() {
    const n = colWidths.length;
    const w = document.body.getBoundingClientRect().width / n;
    setColWidths(colWidths.map((x) => w));
  }

  /*
   * useEffect triggers
   */
  useEffect(() => {
    window.addEventListener("keydown", async (e: KeyboardEvent) => {
      if (e.key === "Escape") {
        setPopupState("none");
        setViewingMenu(false);
      }
    });
    window.addEventListener("mousedown", async (e: MouseEvent) => {
      if (e.button === 0) {
        setViewingMenu(false);
      }
    });
  }, []);

  useEffect(async () => {
    const pre_res = await fetch("http://localhost:50000/read", {
      method: "GET",
      mode: "cors",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({ "cmd_name": "read_todo", args: {} }),
    });
    const res = await pre_res.json();
    if (res.status > 0 && Array.isArray(res.res) && res.res.length > 0) {
      setCols(Object.keys(res.res[0]));
      const n = Object.keys(res.res[0]).length;
      const w = document.body.getBoundingClientRect().width / n;
      setColWidths([...new Array(n).keys()].map((x) => w));
      setRows(res.res);
    } else if (
      res.status > 0 && Array.isArray(res.res) && res.res.length === 0
    ) {
      setCols([]);
      setRows([]);
    }
  }, [tableUpdates]);

  useEffect(async () => {
    const pre_res = await fetch("http://localhost:50000/read", {
      method: "GET",
      mode: "cors",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({ "cmd_name": "read_todo", args: {} }),
    });
    const res = await pre_res.json();
    if (res.status > 0 && Array.isArray(res.res) && res.res.length > 0) {
      setCols(Object.keys(res.res[0]));
      setRows(res.res);
    } else if (
      res.status > 0 && Array.isArray(res.res) && res.res.length === 0
    ) {
      setCols([]);
      setRows([]);
    }
  }, [rowUpdates]);

  useEffect(() => {
    if (insertUpdateFocusRef.current) {
      insertUpdateFocusRef.current.focus();
    }
  }, [insertUpdatePopupOpen]);

  useEffect(() => {
    if (settingsFocusRef.current) {
      settingsFocusRef.current.focus();
    }
  }, [settingsPopupOpen]);

  /*
   * input handlers
   */
  function handleInsertUpdateInput(e: SyntheticEvent, input: string) {
    const newInputs = { ...inputs };
    newInputs[input] = e.target.value;
    setInputs(newInputs);
  }

  function handleSettingsInput(e: SyntheticEvent, i: number) {
    const newColWidths = [...colWidths];
    newColWidths[i] = e.target.value;
    setColWidths(newColWidths);
  }

  function handleSetColWidth(w: number, i: number) {
    const newColWidths = [...colWidths];
    newColWidths[i] = w;
    setColWidths(newColWidths);
  }

  async function handleInsert(e: SyntheticEvent) {
    if (!insertUpdatePopupOpen) {
      return;
    }
    const pre_res = await fetch("http://localhost:50000/write", {
      method: "POST",
      mode: "cors",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({ "cmd_name": "insert_todo", args: inputs }),
    });
    const res = await pre_res.json();
    if (res.status === 1) {
      setRowUpdates(rowUpdates + 1);
      setInputs({
        date: new Date().toISOString().split("T")[0],
        title: "",
        tags: "",
        "more_info": "",
      });
    }
  }

  async function handleDelete(i: number) {
    const pre_res = await fetch("http://localhost:50000/exec_cmd", {
      method: "POST",
      mode: "cors",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({ "cmd_name": "delete_todo", args: { rownum: i } }),
    });
    const res = await pre_res.json();
    if (res.status === 1) {
      setRowUpdates(rowUpdates + 1);
    }
  }

  /*
   * our beloved application :)
   */
  return (
    <div>
      <div
        className="App"
        style={{ "opacity": insertUpdatePopupOpen ? 0.5 : 1 }}
      >
        <div id="logo-div">
          <img id="logo-img" src="/src/favicon.jpg" />
          <span className="title">Ischia</span>{" "}
          <input
            type="button"
            value="+"
            accessKey="i"
            onClick={(e) => setPopupState("insertUpdate")}
            id="insertButton"
          />
          <input
            type="button"
            accessKey="s"
            value="Settings"
            onClick={(e) => setPopupState("settings")}
            id="insertButton"
          />
        </div>
        <div>
          <TableView
            rows={rows}
            cols={cols}
            colWidths={colWidths}
            handleSetColWidth={handleSetColWidth}
            handleDelete={handleDelete}
            viewingMenu={viewingMenu}
            setViewingMenu={setViewingMenu}
          >
          </TableView>
        </div>
      </div>
      <InsertUpdatePopup
        if={insertUpdatePopupOpen}
        inputs={inputs}
        handleInsertUpdateInput={handleInsertUpdateInput}
        handleInsert={handleInsert}
        setInsertUpdatePopupOpen={setInsertUpdatePopupOpen}
        ref={insertUpdateFocusRef}
      >
      </InsertUpdatePopup>
      <SettingsPopup
        if={settingsPopupOpen}
        cols={cols}
        colWidths={colWidths}
        handleSettingsInput={handleSettingsInput}
        resetColWidths={resetColWidths}
        setSettingsPopupOpen={setSettingsPopupOpen}
        ref={settingsFocusRef}
      >
      </SettingsPopup>
    </div>
  );
}

export { App };
