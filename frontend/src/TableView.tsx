import { EditPopup } from "./EditPopup.tsx";
import "./TableView.css";
import { ResizableBox } from "react-resizable";
import { Menu } from "./Menu.tsx";
import "./Menu.css";
import { useState } from "react";

function TableView(props) {
  const [menuCoords, setMenuCoords] = useState([0, 0]);
  const [rowModifying, setRowModifying] = useState(-1);

  function handleRowClick(e: SyntheticEvent, i: number) {
    if (e.button === 2) {
      e.preventDefault();
      e.stopPropagation();
      setMenuCoords([
        (e.clientY - e.target.clientTop) + "px",
        (e.clientX - e.target.clientLeft) + "px",
      ]);
      setRowModifying(i);
      props.setViewingMenu(true);
    }
  }

  return (
    <div className="Table-View">
      <div className="Resizable-Table-View">
        {props.cols.map((col, i) => (
          <ResizableBox
            className="custom-box box"
            key={"col-head-" + i}
            axis="x"
            minConstraints={[30, 30]}
            width={props.colWidths[i] || 50}
            height={40}
            resizeHandles={["se"]}
            handle={
              <img className="custom-handle" src="/src/ResizeHandle.svg" />
            }
            handleSize={[8, 8]}
            onResizeStop={(e, d) => {
              props.handleSetColWidth(d.size.width, i);
            }}
          >
            <div className="resiz-inside">
              <span>{col}</span>
            </div>
          </ResizableBox>
        ))}
      </div>

      <table>
        <colgroup>
          {props.colWidths.map((w, i) => (
            <col key={"key-" + i} style={{ width: w + "px" }} />
          ))}
        </colgroup>
        <tbody>
          {props.rows.map((row, i) => (
            <tr className="table-row" key={"row-" + i}>
              {props.cols.map((col, j) => (
                <td
                  key={"col-" + j + "-row-" + i}
                  onClick={(e) => handleRowClick(e, i)}
                  onContextMenu={(e) =>
                    handleRowClick(e, row.id)}
                >
                  {row[col]}
                </td>
              ))}
            </tr>
          ))}
        </tbody>
      </table>
      <Menu
        if={props.viewingMenu}
        coords={menuCoords}
        menuitems={[{ text: "Edit", onClick: (e) => console.log(e) }, {
          text: "Delete",
          onClick: (e) => {
            console.log(rowModifying);
            props.handleDelete(rowModifying);
            props.setViewingMenu(false);
          },
        }]}
      >
      </Menu>
    </div>
  );
}

export { TableView };
