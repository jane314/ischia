#!/usr/bin/bash
root=$(dirname "$0")
find $root \( -path "$root*.ts" -o -path "$root*.tsx" -o -path "root*.css" \) -a \( -not -path "$root/frontend/node_modules/*" \) -exec bash -c 'cat {} | wc -l' \; | awk '{s+=$1} END {print s}'
